﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;

namespace IBANchecker
{
    class IbanNumberChecker
    {
        Dictionary<string, int> ibanCountryList = new Dictionary<string, int>()
        {
            {"AL", 28},{"AD", 24},{"AT", 20},{"AZ", 28},{"BE", 16},
            {"BH", 22},{"BA", 20},{"BR", 29},{"BG", 22},{"CR", 21},
            {"HR", 21},{"CY", 28},{"CZ", 24},{"DK", 18},{"DO", 28},
            {"EE", 20},{"FO", 18},{"FI", 18},{"FR", 27},{"GE", 22},
            {"DE", 22},{"GI", 23},{"GR", 27},{"GL", 18},{"GT", 28},
            {"HU", 28},{"IS", 26},{"IE", 22},{"IL", 23},{"IT", 27},
            {"KZ", 20},{"KW", 30},{"LV", 21},{"LB", 28},{"LI", 21},
            {"LT", 20},{"LU", 20},{"MK", 19},{"MT", 31},{"MR", 27},
            {"MU", 30},{"MC", 27},{"MD", 24},{"ME", 22},{"NL", 18},
            {"NO", 15},{"PK", 24},{"PS", 29},{"PL", 28},{"PT", 25},
            {"RO", 24},{"SM", 27},{"SA", 24},{"RS", 22},{"SK", 24},
            {"SI", 19},{"ES", 24},{"SE", 24},{"CH", 21},{"TN", 24},
            {"TR", 26},{"AE", 23},{"GB", 22},{"VG", 24}
           };

        Dictionary<string, string> dictionaryForLetterReplacementWithIntegers = new Dictionary<string, string>() {
            { "A", "10" },{ "B", "11" },{ "C", "12" }, { "D", "13" },
            { "E", "14" },{ "F", "15" },{ "G", "16" },{ "H", "17" },
            { "I", "18" },{ "J", "19" },{ "K", "20" },{ "L", "21" },
            { "M", "22" },{ "N", "23" },{ "O", "24" },{ "P", "25" },
            { "Q", "26" },{ "R", "27" },{ "S", "28" },{ "T", "29" },
            { "U", "30" },{ "V", "31" },{ "W", "32" },{ "X", "33" },
            { "Y", "34" },{ "Z", "35" }
        };


        public bool AreCharactersValid(string enteredAccountNumber)
        {
            if (Regex.IsMatch(enteredAccountNumber, @"^[a-zA-Z0-9]+$")) return true;
            return false;
        }

        public bool IsCountryPrefixAndLenghthCorrect(string enteredAccountNumber)
        {

            string firstTwoChar = new string(enteredAccountNumber.Take(2).ToArray());
            string enteredCountryPrefix = firstTwoChar.ToUpper();
            int enteredNumberLength = enteredAccountNumber.Length;

            bool isCountryPrefixExisting = ibanCountryList.ContainsKey(enteredCountryPrefix);
            if (!isCountryPrefixExisting) return false;

            int countryAccountLength = ibanCountryList[enteredCountryPrefix];
            if (enteredNumberLength != countryAccountLength) return false;

            return true;

        }

        public string MoveFourCharactersToTheEnd(string enteredAccountNumber)
        {
            string firstFourCharacters = enteredAccountNumber.Substring(0, 4);
           
            string withoutFirstSymbols = enteredAccountNumber.Substring(4);
            
            string manipulatedAccountNumber = (withoutFirstSymbols + firstFourCharacters).ToUpper();

            return manipulatedAccountNumber;

        }

        public string ReplaceLettersWithNumbers(string manipulatedAccountNumber)
        {
            string replacementResult = dictionaryForLetterReplacementWithIntegers.Aggregate(manipulatedAccountNumber, (result, s) => result.Replace(s.Key, s.Value));
            return replacementResult;

        }

        public int CalculateReminder(string accountNumberInDigits)
        {

            var remainder = BigInteger.Parse(accountNumberInDigits) % 97;
            return (int)remainder;

        }

        public void CheckSingleAccount()
        {
            Console.WriteLine("Please enter account number");
            string accountNumber = Console.ReadLine();
            if (accountNumber == "q")
                Environment.Exit(0);

            bool isIbanValid = IbanAccountChecks(accountNumber);

            if (isIbanValid)
            {
                Console.WriteLine($"Account number {accountNumber} is  valid");
            }
            else
            {
                Console.WriteLine($"Account number {accountNumber} is  invalid");
            }            
        }


        public void CheckFile(IbanNumberChecker ibanNumberChecker)
        {
            Console.WriteLine("Please enter the path to file or 'q' to quit");
            string pathToFile = Console.ReadLine();

            string root = Path.GetDirectoryName(pathToFile);
            
            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(pathToFile);
            
            string outputFileName = fileNameWithoutExtension + ".out";
           
            string outputPath = root + "\\" + outputFileName;

            List<string> accountNumbers = new List<string>();

            try
            {
                using (StreamReader reader = new StreamReader(pathToFile))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        bool isIbanValid = (IbanAccountChecks(line));
                        if (isIbanValid)
                        {
                            string accountNumberLine = line + ";" + " true";
                            accountNumbers.Add(accountNumberLine);
                        }
                        else
                        {
                            string accountNumberLine = line + ";" + "false";
                            accountNumbers.Add(accountNumberLine);
                        }

                    }
                }
            }
            catch (Exception)
            {

                Console.WriteLine("Please check the path to the file");
            }

            File.WriteAllLines(outputPath, accountNumbers);
            Console.WriteLine($"Please check the output file: {outputPath} ");

        }

        public bool IbanAccountChecks(string accountNumber)
        {

            bool inbanCharacterValid = AreCharactersValid(accountNumber);
            if (!inbanCharacterValid)
            {
                return false;
            }
            
            bool ibanCountryLengthValid = IsCountryPrefixAndLenghthCorrect(accountNumber);
            if (!ibanCountryLengthValid)
            {
                return false;
            }

            string manipulatedInput = MoveFourCharactersToTheEnd(accountNumber);
            
            string ibanAllDigits = ReplaceLettersWithNumbers(manipulatedInput);
            
            long calculatedReminder = CalculateReminder(ibanAllDigits);
           
            if (calculatedReminder != 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
