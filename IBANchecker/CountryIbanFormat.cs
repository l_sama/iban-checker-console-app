﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IBANchecker
{
    class CountryIbanFormat
    {
        public string CountryPrefix { get; set; }
        public int IbanLength { get; set; }
    }
}
