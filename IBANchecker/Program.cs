﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace IBANchecker
{
    class Program
    {
        static void Main(string[] args)
        {

            IbanNumberChecker ibanNumberChecker = new IbanNumberChecker();


            static void PrintMenu()
            {
                Console.WriteLine("*** IBAN account checker ***");
                Console.WriteLine("** Please enter 1 to check single account number or enter 2 to check the file *");
                Console.WriteLine("** If you wish to quit please enter 'q'");
            }


            while (true)
            {
                PrintMenu();

                switch (Console.ReadLine())
                {
                    case "1":
                        ibanNumberChecker.CheckSingleAccount();
                        break;

                    case "2":
                        ibanNumberChecker.CheckFile(ibanNumberChecker);
                        break;
                    case "q":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.Clear();
                        PrintMenu();
                        break;
                }
            }
        }
    }
}


