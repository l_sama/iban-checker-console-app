# IBANchecker
	
IBANchecker is a console aplication that validates bank account numbers presented in The International Bank Account Number (IBAN) format. The application validates account numbers in two modes:   

1. Interactive verification of IBAN numbers. The user is asked to enter an account number and the program finds out if the number is correct.  
2. Checking IBAN numbers from a text file. The user is prompted to enter the file path and name. The program scans the file and verifies the account numbers. Outputs the results to a file with the same name with the .out extension.

  
The structure of the initial file: each line - one account number IBAN. For example:  
>AA051245445454552117989  
>LT647044001231465456  
>LT517044077788877777  
>LT227044077788877777  
>CC051245445454552117989  
>AE460090000000123456789 
   
The example file named iban.txt is added to this repository.    

  

Result file structure: IBAN; valid. For example:  
>AA051245445454552117989; false  
>LT647044001231465456; true  
>LT517044077788877777; true  
>LT227044077788877777; false  
>CC051245445454552117989; false  
>AE460090000000123456789; true  		
		
		
## Instructions    
1. Download the project from this repo. The simplest way to do this is to select Download repository and extract all the files.  
2. Load it into Visual Studio. You can double-click on IBANChecker.sln to do this.  
3. Build it and try it out!  


 